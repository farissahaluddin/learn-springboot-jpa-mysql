package com.faris.edashboard.entity;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "OrderRecieved")
public class OrderRecieved extends KeyEntity {

    private int orderRecieved;
    private String dateRecieved;

    public OrderRecieved() {
    }

    public OrderRecieved(int orderRecieved, String dateRecieved) {
        this.orderRecieved = orderRecieved;
        this.dateRecieved = dateRecieved;
    }

    public int getOrderRecieved() {
        return orderRecieved;
    }

    public void setOrderRecieved(int orderRecieved) {
        this.orderRecieved = orderRecieved;
    }

    public String getDateRecieved() {
        return dateRecieved;
    }

    public void setDateRecieved(String dateRecieved) {
        this.dateRecieved = dateRecieved;
    }
}
