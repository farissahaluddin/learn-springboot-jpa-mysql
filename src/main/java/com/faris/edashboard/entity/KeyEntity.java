package com.faris.edashboard.entity;

import javax.persistence.*;

@MappedSuperclass
public class KeyEntity {

    @Id
    @Column
    private String id;

    public KeyEntity() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
