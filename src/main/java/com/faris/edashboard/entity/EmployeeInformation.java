package com.faris.edashboard.entity;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Date;
@Entity
@Table(name = "EmployeeInformation")
public class EmployeeInformation extends KeyEntity{

    private String _name;
    private String position;
    private String officeLocation;
    private int age;
    private Date starDate;
    private double salary;

    public EmployeeInformation() {
    }

    public EmployeeInformation(String _name, String position, String officeLocation, int age, Date starDate, double salary) {
        this._name = _name;
        this.position = position;
        this.officeLocation = officeLocation;
        this.age = age;
        this.starDate = starDate;
        this.salary = salary;
    }

    public String get_name() {
        return _name;
    }

    public void set_name(String _name) {
        this._name = _name;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getOfficeLocation() {
        return officeLocation;
    }

    public void setOfficeLocation(String officeLocation) {
        this.officeLocation = officeLocation;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public Date getStarDate() {
        return starDate;
    }

    public void setStarDate(Date starDate) {
        this.starDate = starDate;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }
}
