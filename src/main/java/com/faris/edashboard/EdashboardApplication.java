package com.faris.edashboard;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EdashboardApplication {

    public static void main(String[] args) {
        SpringApplication.run(EdashboardApplication.class, args);
    }

}
