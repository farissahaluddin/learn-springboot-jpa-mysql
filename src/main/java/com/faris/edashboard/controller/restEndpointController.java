package com.faris.edashboard.controller;

import com.faris.edashboard.entity.EmployeeInformation;
import com.faris.edashboard.service.DashboardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class restEndpointController {

    @Autowired
    private DashboardService dashboardService;

    @RequestMapping("/employee")
    public List<EmployeeInformation> getAllEmpl(){
        return dashboardService.getAllEmployee();
    }

    @RequestMapping(method = RequestMethod.POST, value = "/employee/add")
    public String saveEmployeeInfo(@RequestBody EmployeeInformation employeeInformation){
        if(dashboardService.addEmployee(employeeInformation)!=null){
            return "employee data saved success!";
        }
        else{
            return "error saving employee info";
        }
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/employee/delete")
    public String deleteEmp(@RequestParam(name = "id", required = true) String id){
        try{
            dashboardService.deleteEmployee(dashboardService.getEmployee(id));
            return "deleted";
        } catch (Exception e){
            return "fail booossss";
        }

    }


}
