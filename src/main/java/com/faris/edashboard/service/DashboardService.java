package com.faris.edashboard.service;

import com.faris.edashboard.entity.*;

import java.util.List;

public interface DashboardService {

    List<CompanyRevenue> getTodayRevenueDash();
    List<ProductCategory> getBestCategory();
    List<OrderRecieved> getAllOrderRecieved();
    List<OrderCollectionStatus> getOrderCollection();

    List<EmployeeInformation> getAllEmployee();
    EmployeeInformation addEmployee(EmployeeInformation employeeInformation);
    EmployeeInformation updateEmployee(EmployeeInformation employeeInformation);
    void deleteEmployee(EmployeeInformation employeeInformation);

    EmployeeInformation getEmployee(final String id);
}
